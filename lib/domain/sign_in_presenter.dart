import 'package:test_project/domain/utils.dart';
import '../data/requests.dart';

class SignInPresenter {
  Future<void> pressButton(String email, String password,
      Function(void) onResponse, Future<void> Function(String) onError) async {
      if (password.isEmpty) {
        onError('Password is empty');
      }
      else {
        requestSignIn() async {
          await signIn(email, password);
        }
        await requests(requestSignIn, onResponse, onError);

    }
  }
}