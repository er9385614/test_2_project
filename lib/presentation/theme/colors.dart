import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color text;
  abstract final Color hint;
  abstract final Color accent;
  abstract final Color red;
  abstract final Color subTextLight;
  abstract final Color subTextDark;
  abstract final Color background;
  abstract final Color block;
  abstract final Color disable;
}

class ThemeColors extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => const Color(0x48B2E7);

  @override
  // TODO: implement background
  Color get background => const Color(0xF7F7F9);

  @override
  // TODO: implement block
  Color get block => const Color(0xFFFFFF);

  @override
  // TODO: implement disable
  Color get disable => const Color(0x2B6B8B);

  @override
  // TODO: implement hint
  Color get hint => const Color(0x6A6A6A);

  @override
  // TODO: implement red
  Color get red => const Color(0xF87265);

  @override
  // TODO: implement subTextDark
  Color get subTextDark => const Color(0x707B81);

  @override
  // TODO: implement subTextLight
  Color get subTextLight => const Color(0xD8D8D8);

  @override
  // TODO: implement text
  Color get text => const Color(0x2B2B2B);

}