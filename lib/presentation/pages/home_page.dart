import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:test_project/presentation/pages/tabs/favorite.dart';
import 'package:test_project/presentation/pages/tabs/home.dart';
import 'package:test_project/presentation/pages/tabs/notification.dart';
import 'package:test_project/presentation/pages/tabs/profile.dart';
import 'package:test_project/presentation/widgets/bottom_navigation_bar.dart';
import 'package:test_project/presentation/widgets/custom_card.dart';

class HomePage extends StatefulWidget{
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var pages = [HomeTab(), FavoriteTab(), NotificationTab(), ProfileTab()];
  var currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          pages[currentIndex],
          Align(
            alignment: Alignment.bottomCenter,
            child: BottomNavBar(onSelect: (newIndex){
              setState(() {
                currentIndex = newIndex;
              });
            }, onTapCart: (){

            }),
          )
        ],
      ),
    );
  }
}