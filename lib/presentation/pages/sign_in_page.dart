import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_project/domain/sign_in_presenter.dart';
import 'package:test_project/presentation/pages/home_page.dart';
import 'package:test_project/presentation/utils/dialogs.dart';
import 'package:test_project/presentation/widgets/custom_text_filed.dart';

class SignInPage extends StatefulWidget{
  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  SignInPresenter presenter = SignInPresenter();

  var email = TextEditingController(text: 'zxc@gmail.com');
  var password = TextEditingController();
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20
        ),
        child: Column(
          children: [
            SizedBox(height: 121,),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Привет!",
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 32,
                        fontWeight: FontWeight.bold
                      )
                  ),),
                SizedBox(height: 8,),
                Text("Заполните Свои данные или\nпродолжите через социальные медиа",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                    )
                  ),
                ),
              ],
            ),
            SizedBox(height: 8,),
            CustomTextField(
                label: 'Email',
                hint: 'xyz@gmail.com',
                controller: email
            ),
            CustomTextField(
                label: 'Пароль',
                hint: '••••••••',
                controller: password
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: FilledButton(
                    style: FilledButton.styleFrom(

                    ),
                      onPressed: (){
                        presenter.pressButton(
                            email.text,
                            password.text, (_) => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (_) => HomePage()
                        )
                      ),
                                (error) => showError(context, error)
                        );
                      },
                      child: Text('')),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}