import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomNavBar extends StatefulWidget {

  final Function(int) onSelect;
  final Function() onTapCart;

  const BottomNavBar({
    super.key,
    required this.onSelect,
    required this.onTapCart
  }
);

  @override
  State<BottomNavBar> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {

  var currentIndex = 0;

  void onTap(int newIndex){
    setState(() {
      currentIndex = newIndex;
    });
    widget.onSelect(currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 106.w,
      width: double.infinity,
      child: Stack(
          children: [
            Transform.translate(
              offset: Offset(0, -1.5.w),
              child: ImageFiltered(
                imageFilter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
                child: SizedBox(
                  width: double.infinity,
                  child: SvgPicture.asset(
                    "assets/background.svg",
                    fit: BoxFit.fill,
                    color: const Color(0x1F83AAD1),
                  ),
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(0, 4.w),
              child: ImageFiltered(
                imageFilter: ImageFilter.blur(sigmaY: 15, sigmaX: 15),
                child: SizedBox(
                  width: double.infinity,
                  child: SvgPicture.asset(
                    "assets/background.svg",
                    fit: BoxFit.fill,
                    color: const Color(0x26000000),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  width: double.infinity,
                  height: 106.w,
                  child: SvgPicture.asset("assets/background.svg", fit: BoxFit.fill)
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 30.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () => onTap(0),
                          child: SvgPicture.asset("assets/home.svg",
                            color: Color( (currentIndex == 0) ? 0xFF48B2E7 : 0xFF707B81),
                            width: 24.w,
                            height: 24.w,
                          ),
                        ),
                        SizedBox(width: 40.w),
                        GestureDetector(
                          onTap: () => onTap(1),
                          child: SvgPicture.asset("assets/favorite.svg",
                            color: Color( (currentIndex == 1) ? 0xFF48B2E7 : 0xFF707B81),
                            width: 24.w,
                            height: 24.w,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(width: 138.w),
                    Row(
                      children: [
                        GestureDetector(
                            onTap: () => onTap(2),
                            child: SvgPicture.asset("assets/notification.svg",
                              color: Color( (currentIndex == 2) ? 0xFF48B2E7 : 0xFF707B81),
                              width: 24.w,
                              height: 24.w,
                            )),
                        SizedBox(width: 40.w),
                        GestureDetector(
                          onTap: () => onTap(3),
                          child: SvgPicture.asset("assets/profile.svg",
                            color: Color( (currentIndex == 3) ? 0xFF48B2E7 : 0xFF707B81),
                            width: 24.w,
                            height: 24.w,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: widget.onTapCart,
                child: Padding(
                  padding: EdgeInsets.only(bottom: 50.w),
                  child: Container(
                    width: 56.w,
                    height: 56.w,
                    padding: const EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        color: const Color(0xFF48B2E7),
                        borderRadius: BorderRadius.circular(30.w),
                        boxShadow: [
                          BoxShadow(
                              color: const Color(0x995B9EE1),
                              blurRadius: 24,
                              offset: Offset(0, 8.w)
                          )
                        ]
                    ),
                    child: SvgPicture.asset("assets/bag.svg", color: Colors.white, width: 24.w, height: 24.w),
                  ),
                ),
              ),
            ),
          ]
      ),
    );
  }
}
