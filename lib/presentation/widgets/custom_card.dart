import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_project/data/models/model_product.dart';

class ItemProduct extends StatefulWidget {

  final ModelProduct product;
  final bool isFavorite;
  final bool isCart;
  final Function()? onTap;
  final Function()? onTapFavorite;
  final Function()? onTapBasket;

  const ItemProduct({super.key, required this.product, this.onTap, required this.isFavorite, required this.isCart, this.onTapFavorite, this.onTapBasket});

  @override
  State<ItemProduct> createState() => _ItemProductState();
}

class _ItemProductState extends State<ItemProduct> {
  @override
  Widget build(BuildContext context) {
    var texts = Theme.of(context).textTheme;
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16)
      ),
      child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 12.w, left: 12.w),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                        padding: EdgeInsets.symmetric(horizontal: 4.w),
                        child: SizedBox(
                          width: double.infinity,
                          child: AspectRatio(
                            aspectRatio: 159.92/87.85,
                            child: CachedNetworkImage(
                              imageUrl: widget.product.covers[0],
                              fit: BoxFit.cover,
                            ),
                          ),
                        )
                    ),
                    SizedBox(height: 16.w),
                    Text(
                        "Best Seller".toUpperCase(),
                        style: texts.labelMedium!
                    ),
                    SizedBox(height: 4.w),
                    Text(
                        widget.product.title,
                        style: texts.titleMedium?.copyWith(
                            fontSize: 16.sp
                        ),
                        maxLines: 1
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                                child: Text(
                                    "₽${widget.product.cost.toStringAsFixed(2)}",
                                    style: texts.labelSmall
                                )
                            ),
                            GestureDetector(
                              onTap: widget.onTapBasket,
                              child: Container(
                                width: 34.5.w,
                                height: 35.5.w,
                                padding: EdgeInsets.all(10.w),
                                decoration: BoxDecoration(
                                    color: const Color(0xFF48B2E7),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(14.w),
                                        bottomRight: Radius.circular(14.w)
                                    )
                                ),
                                child: SvgPicture.asset((widget.isCart)
                                    ? "assets/korsina.svg"
                                    : "assets/add.svg"
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ]),
            ),
            Padding(
              padding: EdgeInsets.only(top: 3.w, left: 9.w),
              child: GestureDetector(
                onTap: widget.onTapFavorite,
                child: Container(
                  decoration: BoxDecoration(
                      color: const Color(0xFFF7F7F9),
                      borderRadius: BorderRadius.circular(28.w)
                  ),
                  padding: EdgeInsets.all(8.w),
                  child: SvgPicture.asset(
                    (widget.isFavorite)
                        ? "assets/heart.svg"
                        : "assets/heart_no.svg",
                    width: 16.w,
                    height: 16.w,
                  ),
                ),
              ),
            ),
          ]
      ),
    );
  }
}