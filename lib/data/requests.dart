import 'package:supabase_flutter/supabase_flutter.dart';
import '../main.dart';

Future<void> signIn(String email, String password)async{
  final AuthResponse res =  await supabase.auth.signInWithPassword(
    email: email,
    password: password,
  );

}