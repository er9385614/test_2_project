import 'package:flutter_test/flutter_test.dart';
import 'package:test_project/domain/data_validator.dart';

void main() {
  group("", () {
    test("bad-validation", () {
      var validator = DataValidator();
      expect(false, validator.validEmail("NOT_VALID"));
      expect(false, validator.validPassword(""));
    });

    test("good-validation", () {
      var validator = DataValidator();
      expect(true, validator.validEmail("zxc@gmail.com"));
      expect(true, validator.validPassword("123456"));
    });
  });
}
